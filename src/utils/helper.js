const apiBaseURL = "https://5fc5e0e44931580016e3c3a6.mockapi.io/api/countries";

export const apiCall = async (url = "", method, body) => {
  const response = await fetch(apiBaseURL + url, {
    method,
    headers: {
      "Content-type": "application/json",
    },
    body
  });
  const data = await response.json();
  if (response.ok) return data;
  throw data;
};
