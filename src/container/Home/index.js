import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ListCountries from "../../components/listCountries";
import { getCountries, getIsLoading } from "../../redux/reducer/countries";
import { fetchCountries, postCountries } from "../../redux/actions/countries";

const Home = () => {
  const dispatch = useDispatch();

  const countries = useSelector(getCountries);
  const isLoading = useSelector(getIsLoading);

  const [searchData, setSearchData] = useState("");
  const [showAddBtn, setShowAddBtn] = useState(false);
  const [filteredCountries, setFilteredCountries] = useState([]);

  useEffect(() => {
    dispatch(fetchCountries());
  }, []);

  useEffect(() => {
    console.log("---store update---");
    if (searchData && countries.length) {
      console.log("---inside---");
      const filtered = countries.filter((country) =>
        country.name.toLowerCase().includes(searchData)
      );
      console.log(filtered);
      setFilteredCountries(filtered);
    } else setFilteredCountries(countries || []);
  }, [countries]);

  const onChange = (e) => {
    const value = e.target.value.toLowerCase();
    const filtered = countries.filter((country) =>
      country.name.toLowerCase().includes(value)
    );
    if (filtered.length < 1 && value) setShowAddBtn(true);
    setFilteredCountries(filtered);
    setSearchData(value);
  };

  const addCountry = () => {
    dispatch(postCountries(searchData));
    setShowAddBtn(false);
  };

  console.log(countries);
  console.log(filteredCountries);

  return (
    <div>
      {isLoading ? (
        "Loading..."
      ) : (
        <div>
          <input
            value={searchData || ""}
            onChange={onChange}
            placeholder="Search Countries"
          />
          {showAddBtn ? (
            <div>
              <p>{`${searchData} not found`}</p>
              <button onClick={addCountry}>Add Country</button>
            </div>
          ) : (
            <ListCountries countries={filteredCountries} />
          )}
        </div>
      )}
    </div>
  );
};

export default Home;
