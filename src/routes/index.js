import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import Home from "../container/Home";

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
