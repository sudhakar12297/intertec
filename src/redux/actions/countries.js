import { apiCall } from "../../utils/helper";

export const fetchCountries = () => {
  return async (dispatch) => {
    dispatch({ type: "GET_COUNTRIES_START" });
    try {
      const countries = await apiCall("", "GET");
      dispatch({ type: "GET_COUNTRIES_SUCCESS", countries });
    } catch (e) {
      dispatch({ type: "GET_COUNTRIES_FAILURE" });
    }
  };
};

export const postCountries = (searchData) => {
  return async (dispatch, getState) => {
    let { countries } = getState().countries;
    dispatch({ type: "POST_COUNTRIES_START" });
    try {
      dispatch({
        type: "GET_COUNTRIES_SUCCESS",
        countries: [...countries, { id: Math.random(), name: searchData }],
      });
    } catch (e) {
      console.log(e);
      dispatch({ type: "POST_COUNTRIES_FAILURE" });
    }
  };
};
