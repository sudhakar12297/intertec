const initialState = {
  countries: [],
  isLoading: true,
  isError: false,
};

const countries = (state = initialState, action) => {
  const { type, countries } = action;
  switch (type) {
    case "GET_COUNTRIES_START":
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case "GET_COUNTRIES_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        countries,
      };
    case "GET_COUNTRIES_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export const getCountries = (state) => state.countries.countries;
export const getIsLoading = (state) => state.countries.isLoading;
export const getIsError = (state) => state.countries.isError;

export default countries;
