import React from "react";

const DisplayCountries = (props) => {
  const { countries } = props;
  return (
    <>
      {countries.length > 0 ? (
        <select>
          {countries.map((country) => (
            <option id={country.id}>{country.name}</option>
          ))}
        </select>
      ) : (
        <p>No coutries to list</p>
      )}
    </>
  );
};

export default DisplayCountries;
